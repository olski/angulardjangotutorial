import { Component } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ApiService]
})
export class AppComponent {
  movies = [{title: "test"}];
  selectedMovie;

  constructor(private api: ApiService){
    this.getMovies();
    this.selectedMovie = {id: -1, desc: '', year: 0, title: ''};
  }

  getMovies = () =>{
    this.api.getAllMovies().subscribe(data =>{
      this.movies = data;
    },
    error => {
      console.log(error);
    });
  }

  movieClicked = (movie) => {
    this.api.getOneMovie(movie.id).subscribe(
      data =>{
        this.selectedMovie = data
        console.log(data);
    },
    error => {
      console.log(error);
    });

  }

  updateMovie = () => {
    console.log(this.selectedMovie);
    this.api.updateMovie(this.selectedMovie).subscribe(
      data =>{
        console.log(data);
        this.selectedMovie = data;
        this.getMovies();
        this.selectedMovie = {id: -1, desc: '', year: 0, title: ''};
    },
    error => {
      console.log(error);
    });
  }
  

  createMovie = () => {
    console.log(this.selectedMovie);
    this.api.createMovie(this.selectedMovie).subscribe(
      data =>{
        console.log(data);
        this.movies.push(data);
        this.selectedMovie = {id: -1, desc: '', year: 0, title: ''};
    },
    error => {
      console.log(error);
    });
  }

  deleteMovie = () => {
    console.log(this.selectedMovie);
    this.api.deleteMovie(this.selectedMovie.id).subscribe(
      data =>{
        console.log(data);
        this.getMovies();
        this.selectedMovie = {id: -1, desc: '', year: 0, title: ''};
    },
    error => {
      console.log(error);
    });
  }

}

